count = 1
while count < 101 do
    if count < 1
        break;
    else
        if count.modulo(3).zero? &&  count.modulo(5).zero?
            # fizzbuzz
            print "FizzBuzz, "
        elsif count.modulo(5).zero?
            # buzz 5
            if count == 100
                print "Buzz!"
            else
                print "Buzz, "
            end
        elsif
            # fizz 3
            count.modulo(3).zero?
            print "Fizz, "
        else
            # everything else
            print count.to_s + ", "
        end
    end
    count+= 1
 end