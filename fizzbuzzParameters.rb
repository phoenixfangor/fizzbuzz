# Everett says, "Now parameterize it!"
#     Take inputs for range, fizz and buzz.

def getCounts()
# begin
    idealInput = 0
    while idealInput < 1 do
        puts "Please specify your starting number: "
        countStart = gets
        x = 0
        while x < 1
            begin
                countStart = Float(countStart.chomp)
                puts "countStart: " + countStart.to_s
                idealInput = 1
                x = 1
            rescue
                puts "Please enter a starting number: "
                countStart = gets
            end
        end
        
        puts "Please specify your ending number: "
        countStop = gets
        x = 0
        while x < 1
            begin
                countStop = Float(countStop.chomp)
                puts "countStop: " + countStop.to_s
                idealInput = 1
                x = 1 
            rescue
                puts "Please enter an ending number: "
                countStop = gets
            end
        end

        if countStop < countStart
            puts "Oops! Your ending number is smaller than your starting number. Please enter a number greater than " + countStart.to_s
            countStop = gets
            x = 0
            while x < 1
                begin
                    countStop = Float(countStop.chomp)
                    puts "countStop: " + countStop.to_s            
                    idealInput = 1
                    x = 1 
                rescue
                    puts "Please enter an ending number greater than #{countStart.to_s}: "
                    countStop = gets
                end
            end
        else 
            idealInput = 2
        end
    end
    return countStart, countStop
end

def getFizz()
    idealInput = 0
    while idealInput < 1 do
        puts "Please specify your Fizz number: "
        fizz = gets
        x = 0
        while x < 1
            begin
                fizz = Float(fizz.chomp)
                puts "fizz: " + fizz.to_s
                idealInput = 1
                x = 1
            rescue
                puts "Please specify a number for Fizz: "
                fizz = gets
            end
        end 
    end
    return fizz
end

def getBuzz()
    idealInput = 0
    while idealInput < 1 do
        puts "Please specify your Buzz number: "
        buzz = gets
        x = 0
        while x < 1
            begin
                buzz = Float(buzz.chomp)
                puts "buzz: " + buzz.to_s
                idealInput = 1
                x = 1
            rescue
                puts "Please specify a number for buzz: "
                buzz = gets 
            end
        end
    end
    return buzz
end 

puts "Welcome to Custom FizzBuzz. Where you specify your range, your fizz number and your buzz number!"
countArray = getCounts()
countStart = countArray[0].to_f 
countStop = (countArray[1].to_f + 1)
fizz = getFizz().to_f
buzz = getBuzz().to_f

# TO DO: re-prompt user to fizz or buzz outside from count range

while (countStart < countStop) do
    # if countStart < countStart
    #     break;
    # else
        if countStart.modulo(fizz).zero? &&  countStart.modulo(buzz).zero?
            # fizzbuzz
            if countStart == (countStop - 1)
                print "FizzBuzz."
            else
                print "FizzBuzz, "
            end
        elsif countStart.modulo(buzz).zero?
            # buzz
            if countStart == (countStop - 1)
                print "Buzz."
            else
                print "Buzz, "
            end
        elsif countStart.modulo(fizz).zero?
            # fizz
            if countStart == (countStop - 1)
                print "Fizz."
            else
                print "Fizz, "
            end
        else
            # everything else
            if countStart == (countStop - 1)
                print countStart.to_s + "." 
            else
                print countStart.to_s + ", "
            end
        end
    # end
    countStart+= 1
end